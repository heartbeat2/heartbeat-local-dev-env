--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`guid`, `description`, `max_timeout`, `last_seen`, `type`, `alert_sent_mail`, `alert_sent_discord`, `alert_enabled`) VALUES
('ede88b30-1ba0-431a-9775-acfdf2ac0f57', 'client-1', 15, NULL, NULL, 0, 0, 1),
('2d15c391-6646-44f3-8ed7-604ab60cddb5', 'client-2', 60, NULL, NULL, 0, 0, 1);
